angular.module('controller.product', [])

.controller('productController', function($scope, $http, $routeParams, $timeout, User, Product, Distributor, Setting, Config) {

    var product_id = $routeParams.id;
    var product_response = {}; // Full details of main product
    var similar_product_responses = []; // List of similar product with full details

    // Interval
    var interval = Setting.get('interval');
    if (interval === null) { interval = Config.DEFAULT_INTERVAL; }
    $scope.interval = interval;

    // Distributor ids
    var distributor_ids = Setting.get('distributor_ids');
    if (distributor_ids === null) { distributor_ids = []; }

    // Get list of distributors
    Distributor.getAll().then(function(response){
        $scope.distributors = response.distributors;
        for(var i=0; i < $scope.distributors.length; i++) {
            if (distributor_ids.indexOf($scope.distributors[i].id) >= 0) {
                $scope.distributors[i].selected = true;
            }
        }
    }, function(error){
        console.log(error);
    });

    $scope.reloadInterval = function() {
        Setting.set('interval', $scope.interval);
        Product.getById(product_id, $scope.interval).then(function(response){
            product_response = response;
            // Now get similar products
            getSimilarProducts();
            showProductOnGraph();
        }, function(error){
            console.log(error);
        });
    };

    // Get product details
    Product.getById(product_id, $scope.interval).then(function(response){
        $scope.product = response.product;
        $scope.product.price = response.prices[response.prices.length - 1].price;
        $scope.distributor = response.distributor;
        product_response = response;

        // Now get similar products
        getSimilarProducts();
        showProductOnGraph();
    }, function(error){
        console.log(error);
    });

    function getSimilarProducts() {
        $scope.similarProducts = [];
        var distributor_ids = [];
        for(var i=0; i < $scope.distributors.length; i++) {
            if ($scope.distributors[i].selected) {
                distributor_ids.push($scope.distributors[i].id);
            }
        }
        Setting.set('distributor_ids', distributor_ids);
        Product.getSimilarProducts(product_id, distributor_ids).then(function(response){
            for(var i=0; i < response.length; i++) {
                response[i].isGraphed = 0;
                response[i].prices = {};
                $scope.similarProducts.push(response[i]);
            }
        }, function(error){
            console.log(error);
        });
    }


    $scope.switchRetailer = function(index) {
        $scope.distributors[index].selected = !$scope.distributors[index].selected;

        getSimilarProducts();
    }



    $scope.getBtnClass = function(index) {
        var btnClass = '';
        switch(index) {
            case 0: btnClass = 'primary'; break;
            case 1: btnClass = 'danger'; break;
            case 2: btnClass = 'success'; break;
            case 3: btnClass = 'info'; break;
            case 4: btnClass = 'warning'; break;
            default: btnClass = ''; break;
        }
        return btnClass;
    }

    $scope.addPrices = function(p_id) {
        for(var i=0; i < $scope.similarProducts.length; i++) {
            if ($scope.similarProducts[i].id == p_id) {
                // Mark as graphed
                $scope.similarProducts[i].isGraphed = 1;
                // Add to the list for graphing
                Product.getById(p_id, $scope.interval).then(function(response){
                    similar_product_responses.push(response);
                    showSimilarProductsOnGraph();
                }, function(error){
                    console.log(error);
                });
            }
        }
    };

    $scope.removePrices = function(p_id) {
        console.log($scope.btnClass);
        for(var i=0; i < $scope.similarProducts.length; i++) {
            if ($scope.similarProducts[i].id == p_id) {
                $scope.similarProducts[i].isGraphed = 0;
            }
        }
        for(var i=0; i < similar_product_responses.length; i++) {
            if (similar_product_responses[i].product.id == p_id) {
                similar_product_responses.splice(i, 1);
            }
        }
        showSimilarProductsOnGraph();
    };

    function showProductOnGraph() {
        var rows = [];
        for(var i=0; i < product_response.prices.length; i++) {
            var timestamp = new Date(product_response.prices[i].date);
            rows.push({
                c: [
                    {v: timestamp},
                    {v: product_response.prices[i].price},
                    {v: '$' + product_response.prices[i].price}
                ]
            });
            if (i < product_response.prices.length - 1) {
                rows.push({
                    c: [
                        {v: timestamp},
                        {v: product_response.prices[i+1].price},
                        {v: '$' + product_response.prices[i+1].price}
                    ]
                })
            }
        }

        $scope.chartObject = {};

        $scope.chartObject.data = {"cols": [
            {id: "t", label: "Date", type: "date"},
            {id: "s", label: product_response.product.name, type: "number"},
            {id: "x", label: "", type: "string", role: "tooltip"}
        ], "rows": rows};

        $scope.chartObject.type = 'LineChart';
        $scope.chartObject.options = {
            chartArea: {
            },
            backgroundColor: {
                stroke: '#ccc',
                strokeWidth: 0
            },
            hAxis: {
            },
            vAxis: {
                baseline: 0,
                format: 'currency',
                gridlines: {
                    count: -1
                }
            },
            legend: {
                position: 'bottom',
            },
            colors: ['#666']
        }
    }

    function getPriceDates() {
        $scope.labels = [];
        for(var i=0; i < product_response.prices.length; i++) {
            $scope.labels.push(product_response.prices[i].date);
        }
    }


    function showSimilarProductsOnGraph() {
        var rows = [];
        for(var i=0; i < product_response.prices.length; i++) {
            var timestamp = new Date(product_response.prices[i].date);
            var c = [
                    {v: timestamp},
                    {v: product_response.prices[i].price},
                    {v: '$' + product_response.prices[i].price}
                ];
            for(var j=0; j < similar_product_responses.length; j++) {
                c.push({v: similar_product_responses[j].prices[i].price});
                c.push({v: '$' + similar_product_responses[j].prices[i].price});
            }
            rows.push({
                c: c
            });
            if (i < product_response.prices.length - 1) {
                d = [
                    {v: timestamp},
                    {v: product_response.prices[i+1].price},
                    {v: '$' + product_response.prices[i].price}
                ];
                for(var j=0; j<similar_product_responses.length; j++) {
                    d.push({v: similar_product_responses[j].prices[i+1].price });
                    d.push({v: '$' + similar_product_responses[j].prices[i].price});
                }
                rows.push({
                    c: d
                })
            }
        }

        var cols = [];
        cols = [
            {id: "t", label: "Date", type: "date"},
            {id: "s", label: product_response.product.name, type: "number"},
            {id: "z", label: "", type: "string", role: "tooltip"}
        ];
        for(var i=0;i<similar_product_responses.length;i++) {
            cols.push({id: "x" + i, label: similar_product_responses[i].product.name, type: "number"});
            cols.push({id: "z" + i, label: "", type: "string", role: "tooltip"});
        }

        $scope.chartObject = {};

        $scope.chartObject.data = {"cols": cols, "rows": rows};

        $scope.chartObject.type = 'LineChart';
        $scope.chartObject.options = {
            chartArea: {
            },
            backgroundColor: {
                stroke: '#ccc',
                strokeWidth: 0
            },
            hAxis: {
            },
            vAxis: {
                baseline: 0,
                format: 'currency',
                gridlines: {
                    count: -1
                }
            },
            legend: {
                position: 'bottom',
            },
            colors: ['#666', '#2e6da4', '#d43f3a', '#4cae4c', '#46b8da', '#eea236']
        }
    };

    $scope.isOpen = false;

    $scope.toggleAlertPrice = function() {
        $scope.isOpen = !$scope.isOpen;
    };

    $scope.alertPopover = {
        content: '',
        templateUrl: 'alertPopover.html',
        title: ''
    };



    User.getWatchedProduct({
        product_id: product_id
    }).then(function(response){
        $scope.watch = response;
        console.log($scope.watch);
    }, function(error){
        console.log(error);
    });

    $scope.watched = false;

    $scope.watchProduct = function(price) {
        User.watchProduct({
            product_id: product_id,
            price: price
        }).then(function(response){
            $scope.watch.watched = true;
            $scope.watch.price = price;
            if (price > 0) {
                $scope.watched = true;
            }
            $timeout(function(){
                $scope.isOpen = false;
            }, 1000);
        }, function(error){
            console.log(error);
        });
    };

    $scope.unwatchProduct = function() {
        User.unwatch({
            product_id: product_id
        }).then(function(){
            $scope.isOpen = false;
            $scope.watch.watched = false;
            $scope.watch.price = null;
        }, function(error){
            console.log(error);
        });
    }

});
