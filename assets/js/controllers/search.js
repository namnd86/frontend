angular.module('controller.search', [])

.controller('searchController', function($scope, $http, $routeParams, Product, Distributor, Config, Slug, Setting) {
    var page = $routeParams.page ? $routeParams.page : 0;
    var keyword = $routeParams.keyword;
    $scope.page = 'search';

    // Settings toggle option
    var show_settings = Setting.get('show_settings');
    if (show_settings === null) { show_settings = true; }
    $scope.show_settings = show_settings;
    $scope.toggleSettings = function(){
        $scope.show_settings = !$scope.show_settings;
        Setting.set('show_settings', $scope.show_settings);
    };

    // Items per page
    $scope.per_pages = [10, 20, 50];
    $scope.per_page = Setting.get('per_page');
    if (!$scope.per_page) { $scope.per_page = Config.PRODUCTS_PER_PAGE; }
    $scope.changePerPage = function(per_page) {
        $scope.params.items_per_page = per_page;
        $scope.itemsPerPage = per_page;
        Setting.set('per_page', per_page);
        searchProducts();
    };

    // Pagination
    $scope.itemsPerPage = $scope.per_page;
    $scope.pagination = { current: 1 };
    $scope.pageChanged = function(newPage) {
        $scope.params['page'] = newPage - 1; // Convert to index
        searchProducts();
    };
    // Sort type
    $scope.sort_types = [
        { value: 'similarity desc', name: 'Most Relevant'},
        { value: 'current_price asc', name: 'Price: Low to High'},
        { value: 'current_price desc', name: 'Price: High to Low'},
        { value: 'relative_diff asc', name: 'Most Drop: Percentage'},
        { value: 'absolute_diff asc', name: 'Most Drop: Dollar Amount'},
        { value: 'relative_diff desc', name: 'Most Rise: Percentage'},
        { value: 'absolute_diff desc', name: 'Most Rise: Dollar Amount'}
    ];
    $scope.sort_type = 'similarity desc';
    $scope.changeSortType = function(sort_type) {
        $scope.params['sort_type'] = sort_type;
        Setting.set('sort_type', sort_type);
        searchProducts();
    };
    $scope.getSortTypeName = function(value) {
        for(var i=0; i < $scope.sort_types.length; i++) {
            if ($scope.sort_types[i].value == value) {
                return $scope.sort_types[i].name;
            }
        }
    };

    // Interval
    var interval = Setting.get('interval');
    if (interval === null) { interval = Config.DEFAULT_INTERVAL; }

    // Price range
    var price_from = Setting.get('price_from');
    var price_to = Setting.get('price_to');

    // Distributor ids
    var distributor_ids = Setting.get('distributor_ids');
    if (distributor_ids === null) { distributor_ids = []; }


    $scope.params = {
        'page': page,
        'keyword': keyword,
        'sort_type': $scope.sort_type,
        'interval': interval,
        'distributor_ids': distributor_ids,
        'price_from': price_from,
        'price_to': price_to,
        'items_per_page': $scope.per_page,
    };

    // Pagination
    $scope.itemsPerPage = Config.PRODUCTS_PER_PAGE;
    $scope.pagination = {
        current: 1
    };
    $scope.pageChanged = function(newPage) {
        $scope.params['page'] = newPage - 1; // Convert to index
        searchProducts();
    };


    searchProducts();



    function searchProducts() {
        //console.log($scope.params);
        Product.searchProducts($scope.params).then(function(response){
            //console.log(response);
            var pages = [];
            for(var i=0; i < response.num_pages; i++) {
                pages.push(i);
            }
            $scope.totalItems = response.total_items;
            $scope.pages = pages;
            $scope.products = response.products;
            window.scrollTo(0, 0);
        }, function(error){
            console.log(error);
        });
    };

    $scope.refine = function() {
        $scope.params.distributor_ids = [];
        for(var i=0; i < $scope.distributors.length; i++) {
            if ($scope.distributors[i].selected) {
                $scope.params.distributor_ids.push($scope.distributors[i].id);
            }
        }
        Setting.set('interval', $scope.params.interval);
        Setting.set('distributor_ids', $scope.params.distributor_ids);
        Setting.set('price_from', $scope.params.price_from);
        Setting.set('price_to', $scope.params.price_to);

        searchProducts();
    };

    Distributor.getAll().then(function(response){
        $scope.distributors = response.distributors;
    }, function(error){
        console.log(error);
    });

});
