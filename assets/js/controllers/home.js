angular.module('controller.home', [])

.controller('homeController', function($scope, $http, $routeParams, Product, Distributor, Slug, Config, Setting) {
    $scope.page = 'home';
    // Settings toggle option
    var show_settings = Setting.get('show_settings');
    if (show_settings === null) { show_settings = true; }
    $scope.show_settings = show_settings;
    $scope.toggleSettings = function(){
        $scope.show_settings = !$scope.show_settings;
        Setting.set('show_settings', $scope.show_settings);
    };

    // Items per page
    $scope.per_pages = [10, 20, 50];
    $scope.per_page = Setting.get('per_page');
    if (!$scope.per_page) { $scope.per_page = Config.PRODUCTS_PER_PAGE; }
    $scope.changePerPage = function(per_page) {
        $scope.params.items_per_page = per_page;
        $scope.itemsPerPage = per_page;
        Setting.set('per_page', per_page);
        getTopDrop();
    };

    // Pagination
    $scope.itemsPerPage = $scope.per_page;
    $scope.pagination = { current: 1 };
    $scope.pageChanged = function(newPage) {
        $scope.params['page'] = newPage - 1; // Convert to index
        getTopDrop();
    };

    // Sort type
    $scope.sort_types = [
        { value: 'relative_diff asc', name: 'Drop: Percentage'},
        { value: 'absolute_diff asc', name: 'Drop: Dollar Amount'},
        { value: 'current_price asc', name: 'Price: Low to High'},
        { value: 'current_price desc', name: 'Price: High to Low'}
    ];
    $scope.sort_type = Setting.get('sort_type');
    if (!valid_sort_type($scope.sort_type)) { $scope.sort_type = Config.DEFAULT_SORT_TYPE; }
    $scope.getSortTypeName = function(value) {
        for(var i=0; i < $scope.sort_types.length; i++) {
            if ($scope.sort_types[i].value == value) {
                return $scope.sort_types[i].name;
            }
        }
    };
    function valid_sort_type(value) {
        var valid = false;
        for(var i=0; i < $scope.sort_types.length; i++) {
            if ($scope.sort_types[i].value == value) {
                valid = true;
            }
        }
        return valid;
    };
    $scope.changeSortType = function(sort_type) {
        $scope.params['sort_type'] = sort_type;
        Setting.set('sort_type', sort_type);
        getTopDrop();
    };

    // Interval
    var interval = Setting.get('interval');
    if (interval === null) { interval = Config.DEFAULT_INTERVAL; }

    // Price range
    var price_from = Setting.get('price_from');
    var price_to = Setting.get('price_to');

    // Distributor ids
    var distributor_ids = Setting.get('distributor_ids');
    if (distributor_ids === null) { distributor_ids = []; }

    // Get the list of distributors
    Distributor.getAll().then(function(response){
        $scope.distributors = response.distributors;
        for(var i=0; i < $scope.distributors.length; i++) {
            if (distributor_ids.indexOf($scope.distributors[i].id) >= 0) {
                $scope.distributors[i].selected = true;
            }
        }
    }, function(error){
        console.log(error);
    });

    // Compare type
    // $scope.compare_type = Setting.get('compare_type');
    // if (!$scope.compare_type) { $scope.compare_type = 'exact'; }
    // $scope.$watch('params.compare_type', function(newValue) {
    //     Setting.set('compare_type', newValue);
    // }, true);

    $scope.params = {
        'page': 0,
        'keyword': '',
        'sort_type': $scope.sort_type,
        'interval': interval,
        'distributor_ids': distributor_ids,
        'price_from': price_from,
        'price_to': price_to,
        'items_per_page': $scope.per_page,
        // 'compare_type': $scope.compare_type
    };



    // Get top drop
    getTopDrop();

    $scope.refine = function() {
        $scope.params.distributor_ids = [];
        for(var i=0; i < $scope.distributors.length; i++) {
            if ($scope.distributors[i].selected) {
                $scope.params.distributor_ids.push($scope.distributors[i].id);
            }
        }
        Setting.set('interval', $scope.params.interval);
        Setting.set('distributor_ids', $scope.params.distributor_ids);
        Setting.set('price_from', $scope.params.price_from);
        Setting.set('price_to', $scope.params.price_to);

        getTopDrop();
    };

    $scope.setDateRange = function(interval) {
        $scope.params.interval = interval;
        Setting.set('interval', $scope.params.interval);
        getTopDrop();
    };
    $scope.setPriceRange = function() {
        Setting.set('price_from', $scope.params.price_from);
        Setting.set('price_to', $scope.params.price_to);
        getTopDrop();
    };
    $scope.setDistributors = function() {
        $scope.params.distributor_ids = [];
        for(var i=0; i < $scope.distributors.length; i++) {
            if ($scope.distributors[i].selected) {
                $scope.params.distributor_ids.push($scope.distributors[i].id);
            }
        }
        Setting.set('distributor_ids', $scope.params.distributor_ids);
        getTopDrop();
    };

    $scope.mobileRefine = function() {
        $scope.isOpen = false;
        $scope.refine();
    };

    $scope.isOpen = false;

    $scope.mobileToggleSettings = function() {
        $scope.isOpen = !$scope.isOpen;
    }

    function getTopDrop() {
        Product.getTopDrop($scope.params).then(function(response){
            var pages = [];
            for(var i=0; i < response.num_pages; i++) {
                pages.push(i);
            }
            $scope.totalItems = response.total_items;
            $scope.pages = pages;
            $scope.products = response.products;
            window.scrollTo(0, 0);
        }, function(error){
            console.log(error);
        });
    };

    $scope.settingPopover = {
        content: '',
        templateUrl: 'settingPopover.html',
        title: ''
    };
});
