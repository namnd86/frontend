angular.module('ptApp')

.controller('accountController', function($rootScope, $scope, $location, $window, Auth, User){

    $scope.logout = function() {
        Auth.logout();
        $location.path('/');
        $window.location.reload();
    };

    User.getWatchedProducts().then(function(products){
        $scope.products = products;
    }, function(error){
        console.log(error);
    });

    $scope.updateDesiredPrice = function(index) {
        User.updateWatchedPrice($scope.products[index]).then(function(desired_price){
            $scope.products[index].desired_price = desired_price;
        }, function(error){
            console.log(error);
        });
    };

    $scope.unwatch = function(index) {
        User.unwatch($scope.products[index]).then(function(){
            $scope.products.splice(index, 1);
        }, function(error){
            console.log(error);
        });
    }
})

.controller('notificationSettingsController', function($scope, User) {
    User.emailSettings().then(function(response){
        $scope.settings = response;
    }, function(error){
        console.log(error);
    });

    $scope.toggleNewsletter = function() {
        $scope.settings.email_newsletter = !$scope.settings.email_newsletter;
        updateEmailSettings();
    };
    $scope.toggleAlert = function() {
        $scope.settings.email_alert = !$scope.settings.email_alert;
        updateEmailSettings();
    };

    function updateEmailSettings() {
        User.updateEmailSettings($scope.settings).then(function(response){

        }, function(error){
            console.log(error);
        });
    };
})
