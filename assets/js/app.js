var ptApp = angular.module('ptApp', [
    'ngRoute',
    'angulartics', 'angulartics.google.analytics',
    'ui.bootstrap',
    'popoverToggle',
    'angular-loading-bar',
    'ngAnimate',
    'googlechart',
    'LocalStorageModule',
    'xeditable',
    'angularUtils.directives.dirPagination',
    'slugifier',
    'angucomplete-alt',
    'angularMoment',
    'model.product',
    'model.distributor',
    'controller.home',
    'controller.search',
    'controller.product'
]);

ptApp.config(function($routeProvider, $locationProvider, $httpProvider, localStorageServiceProvider) {
    $routeProvider
        // home page
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'homeController'
        })

        .when('/account', {
            templateUrl: 'views/account.html',
            controller: 'accountController'
        })
        .when('/account/email-settings', {
            templateUrl: 'views/account/email-settings.html',
            controller: 'notificationSettingsController'
        })

        .when('/contact', {
            templateUrl: 'views/contact.html',
            controller: 'contactController'
        })

        .when('/top-drop/:page', {
            templateUrl: 'views/home.html',
            controller: 'homeController'
        })

        .when('/search/:keyword?/:page?', {
            templateUrl: 'views/home.html',
            controller: 'searchController'
        })

        .when('/p/:id/:any', {
            templateUrl: 'views/product.html',
            controller: 'productController'
        })

        .when('/:any/p/:id', {
            templateUrl: 'views/product.html',
            controller: 'productController'
        });


    // Use the HTML5 History API
    $locationProvider.html5Mode(true);
    localStorageServiceProvider.setPrefix('OZPriceTracker');

    /*$httpProvider.interceptors.push(function($q, localStorageService){
        return {
            'request': function(config) {
                var token = '';
                if(localStorageService.isSupported) {
                    token = localStorageService.get('token');
                } else {
                    token = localStorageService.cookie.get('token');
                }
                config.headers['Token'] = token;
                return config;
            }
        };
    });*/
});

ptApp.run(function($rootScope, $window, Config, User, editableOptions){

    $rootScope.user = {};
    editableOptions.theme = 'bs3';
    $window.fbAsyncInit = function() {
        // Execute when the SDK is loaded
        FB.init({
            appId: Config.FACEBOOK_APP_ID,
            status: true, // Check the authentication status at the start up of the app
            cookie: true, // Enable cookies to allow the server to access
            xfbml: true, // Parse XFBML
            version: 'v2.3'
        });

        User.get();
        $rootScope.user = User;
    };

    // Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

});

ptApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

ptApp.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
});

ptApp.filter('customCurrency', ["$filter", function ($filter) {
    return function(amount, currencySymbol){
        var currency = $filter('currency');

        if(amount < 0){
            return currency(amount, currencySymbol).replace("(", "-").replace(")", "").replace(".00", "");
        } else if (amount > 0) {
            return '+' + currency(amount, currencySymbol).replace(".00", "");
        }
        return currency(amount, currencySymbol).replace(".00", "");
    };
}]);

ptApp.controller('appController', function($rootScope, $scope, $window, $routeParams, $location, Auth, Config, Slug) {


    $scope.base_url = Config.BASE_URL;
    $scope.api_url = Config.API_URL;
    $rootScope.keyword = '';
    $scope.selectItem = function(selected) {
        var product = selected.originalObject;
        //console.log(product);
        window.location = Slug.slugify(product.name) + '/p/' + product.id;
    };

    $scope.inputChanged = function(keyword) {
        $rootScope.keyword = keyword;
    };

    $scope.search = function(keyEvent) {
        //console.log($scope.params);
        if (keyEvent.which === 13) {
            window.location = '/search/' + $rootScope.keyword;
        }
    };

    $scope.getClass = function(path) {
        var cur_path = $location.path().substr(0, path.length);
        if (cur_path == path) {
            if($location.path().substr(0).length > 1 && path.length == 1 )
                return "";
            else
                return "active";
        } else {
            return "";
        }
    };

    $scope.fbLogin = function() {
        Auth.fbLogin("/");
    };


});

ptApp.controller('loginController', function($scope, $http) {

});

ptApp.controller('contactController', function($scope, $http) {

});


ptApp.directive('siteHeader', [function(){
    return {
        restrict: 'E',
        scope: true,
        templateUrl: "views/common/header.html"
    }
}])
