angular.module('ptApp')
.service('Config', [function(){
    return {
        // FACEBOOK_APP_ID: '1003339993031975', // Local dev
        FACEBOOK_APP_ID: '995784717120836', // Live server
        BASE_URL: 'http://electronics.ozpricetracker.com/',
        // API_URL: 'http://127.0.0.1:5000/', // Local dev
        API_URL: 'http://api.ozpricetracker.com/', // Live server
        PRODUCTS_PER_PAGE: 20,
        DEFAULT_INTERVAL: 7,
        DEFAULT_SORT_TYPE: 'relative_diff asc'
    };
}])
