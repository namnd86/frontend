angular.module('ptApp')
.factory('User', function(localStorageService, $http, $q, Config){

    var user = {};
    user.token = null;

    user.logout = function() {
        user = {};
        if (localStorageService.isSupported) {
            localStorageService.clearAll();
        } else {
            localStorageService.cookie.clearAll();
        }
    };

    user.set = function(userData) {
        this.token = userData.token; // For authentication purpose
        this.social_id = userData.social_id; // For UI display purpose
        this.first_name = userData.first_name;
        // Save on local storage
        if (localStorageService.isSupported) {
            localStorageService.set('token', userData.token);
        } else {
            localStorageService.cookie.set('token', userData.token);
        }
    };

    function get_token() {
        if(localStorageService.isSupported) {
            return localStorageService.get('token');
        } else {
            return localStorageService.cookie.get('token');
        }
        return null;
    }

    user.get = function() {
        var _this = this;
        if (_this.token == null) {
            if(localStorageService.isSupported) {
                _this.token = localStorageService.get('token');
            } else {
                _this.token = localStorageService.cookie.get('token');
            }
        }
        if (_this.token != null) {
            $http({
                url: Config.API_URL + 'users/token',
                method: 'POST',
                params: {token: _this.token}
            }).success(function(userData) {
                _this.social_id = userData.social_id;
                _this.first_name = userData.first_name;
            }).error(function(error){
                if (localStorageService.isSupported) {
                    localStorageService.clearAll();
                } else {
                    localStorageService.cookie.clearAll();
                }
                //console.log(error);

            });
        }
    };

    user.emailSettings = function() {
        var q = $q.defer();
        $http({
            url: Config.API_URL + 'users/email-settings',
            method: 'POST',
            params: {token: get_token() }
        }).success(function(response){
            q.resolve(response);
        }).error(function(error){
            q.reject(error);
        });
        return q.promise;
    };

    user.updateEmailSettings = function(data) {
        var q = $q.defer();
        data.token = this.token;
        $http({
            url: Config.API_URL + 'users/update-email-settings',
            method: 'POST',
            params: data
        }).success(function(response){
            q.resolve(response);
        }).error(function(error){
            q.reject(error);
        });
        return q.promise;
    };

    user.watchProduct = function(data) {
        var q = $q.defer();
        data.token = this.token;
        $http({
            url: Config.API_URL + 'users/watch-product',
            method: 'POST',
            params: data
        }).success(function(response){
            q.resolve(response);
        }).error(function(error){
            q.reject(error);
        });
        return q.promise;
    };

    user.getWatchedProduct = function(data) {
        var q = $q.defer();
        data.token = get_token();
        $http({
            url: Config.API_URL + 'users/watched-product',
            method: 'POST',
            params: data
        }).success(function(response) {
            q.resolve(response);
        }).error(function(error){
            q.reject(error);
        });
        return q.promise;
    };

    user.getWatchedProducts = function() {
        var q = $q.defer();
        $http({
            url: Config.API_URL + 'users/watched-products',
            method: 'POST',
            params: {token: get_token() }
        }).success(function(response){
            q.resolve(response.products);
        }).error(function(error){
            q.reject(error);
        });
        return q.promise;
    };

    user.updateWatchedPrice = function(data) {
        var q = $q.defer();
        data.token = this.token;
        $http({
            url: Config.API_URL + 'users/update-watched-price',
            method: 'POST',
            params: data
        }).success(function(response){
            q.resolve(response.desired_price);
        }).error(function(error){
            q.reject(error);
        });
        return q.promise;
    };

    user.unwatch = function(data) {
        var q = $q.defer();
        data.token = this.token;
        $http({
            url: Config.API_URL + 'users/unwatch',
            method: 'POST',
            params: data
        }).success(function(response){
            q.resolve(response);
        }).error(function(error){
            q.reject(error);
        });
        return q.promise;
    }

    return user;
})
