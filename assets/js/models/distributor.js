// Distributor model
angular.module('model.distributor', [])
.factory('Distributor', function($http, $q, Config) {
    var distributors = {distributors:[
        {id: 1, name: "JB Hifi"}, {id: 2, name: "Dicksmith"}, {id: 4, name: "Kogan"}, {id:6, name:"Harvey Norman"},
        {id: 7, name: "Officeworks"}, {id: 9, name: "The Good Guys"}
    ]};
    return {
        getAll: function() {
            var q = $q.defer();
            q.resolve(distributors);
            // $http.get(Config.API_URL + 'distributors')
            // .success(function(response){
            //     q.resolve(response);
            // }).error(function(error){
            //     q.reject(error);
            // });
            return q.promise;
        }
    }
})
