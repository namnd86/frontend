// Product model
angular.module('model.product', [])
.factory('Product', function($http, $q, Config) {
    function parseQS(url, params) {
        var encoded;
        if (params) encoded = $.param(params);
        if (encoded && encoded !== "") {
            return url + "?" + encoded;
        } else {
            return url;
        }
    };

    return {

        getById: function(id, interval) {
            var q = $q.defer();
            $http({
                url: Config.API_URL + 'products/' + id,
                method: 'GET',
                params: {interval: interval}
            })
            // $http.get(Config.API_URL + 'products/' + id)
            .success(function(response) {
                q.resolve(response);
            }).error(function(error){
                q.reject(error);
            });
            return q.promise;
        },


        getSimilarProducts: function(id, distributor_ids) {
            var q = $q.defer();
            $http({
                url: Config.API_URL + 'products/similar-products/' + id,
                method: 'GET',
                params: {distributor_ids: distributor_ids}
            }).success(function(response){
                var products = response.similar_products;
                q.resolve(products);
            }).error(function(error){
                q.reject(error);
            });
            return q.promise;
        },

        getTopDrop: function(params) {
            var q = $q.defer();
            // console.log(parseQS(Config.API_URL + 'products/top-drop', params));
            $http({
                url: Config.API_URL + 'products/top-drop',
                method: 'GET',
                params: params
            }).success(function(response){
                q.resolve(response);
            }).error(function(error){
                q.reject(error);
            });
            return q.promise;
        },

        searchProducts: function(params) {
            var q = $q.defer();
            //console.log(parseQS(Config.API_URL + 'products/search', params));
            $http({
                url: Config.API_URL + 'products/search',
                method: 'GET',
                params: params
            }).success(function(response){
                q.resolve(response);
            }).error(function(error){
                q.reject(error);
            });
            return q.promise;
        }
    }
})
