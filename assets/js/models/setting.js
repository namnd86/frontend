angular.module('ptApp')

.factory('Setting', function(localStorageService) {
    return {
        set: function(key, value) {
            if (localStorageService.isSupported) {
                localStorageService.set(key, value);
            } else {
                localStorageService.cookie.set(key, value);
            }
        },

        get: function(key) {
            if (localStorageService.isSupported) {
                return localStorageService.get(key);
            } else {
                return localStorageService.cookie.get(key);
            }
        }
    };
});
