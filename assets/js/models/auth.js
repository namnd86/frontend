angular.module('ptApp')
.factory('Auth', function($rootScope, $http, $window, Config, User) {
    return {
        fbLogin: function(url) {
            var scope = 'email';
            FB.login(function(response){
                if (response.status === 'connected') {
                    FB.api('/me', function(res) {
                        // console.log(res);
                        $http({
                            url: Config.API_URL + 'auth/connectfb',
                            method: 'POST',
                            params: res
                        }).success(function(userData) {
                            User.set({
                                token: userData.token,
                                social_id: res.id,
                                first_name: res.first_name
                            });
                            $window.location.reload();
                            //console.log(User);
                        }).error(function(error){
                            console.log(error);
                        });
                    })
                }
            }, {scope: scope});
        },

        logout: function() {
            User.logout();
        }
    };
})
